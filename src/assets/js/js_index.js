// responsivePoint
document.querySelector(".dot.dot1").addEventListener('click', function (event) {
  document.querySelector("div.slideShow > div").style.left = '-230px';
});
document.querySelector(".dot.dot2").addEventListener('click', function (event) {
  document.querySelector("div.slideShow > div").style.left = '-460px';
});
document.querySelector(".dot.dot3").addEventListener('click', function (event) {
  document.querySelector("div.slideShow > div").style.left = '-690px';
});
document.querySelector(".dot.dot4").addEventListener('click', function (event) {
  document.querySelector("div.slideShow > div").style.left = '-920px';
});
document.querySelector(".dot.dot5").addEventListener('click', function (event) {
  document.querySelector("div.slideShow > div").style.left = '-0px';
});

// lightBox
var lightbox = document.createElement('div')
lightbox.id = 'lightbox'
document.body.appendChild(lightbox)

var images = document.querySelectorAll('img')
images.forEach(image => {
  image.addEventListener('click', function(e) {
    lightbox.classList.add('active')
    const img = document.createElement('img')
    img.src = image.src
    while (lightbox.firstChild) {
      lightbox.removeChild(lightbox.firstChild)
    }
    lightbox.appendChild(img)
  })
})

lightbox.addEventListener('click',function(e) {
  if (e.target !== e.currentTarget) return
  lightbox.classList.remove('active')
})


// gallery
var galleryResponsive = function () {

  var stt = document.querySelector(".stt");
  var sliderWidth = stt.offsetWidth + 15;
  var slideList = document.querySelector(".gallery");
  var count = 1;
  var items = slideList.querySelectorAll(".stt").length;

  var prevSlide = function () {
    if (count > 1) {
      count = count -2;
      slideList.style.left = "-" + count * sliderWidth + "px";
      count++;
    }
    else if (count = 1) {
      count = items - 3;
      slideList.style.left = "-" + count * sliderWidth + "px";
      count++;
    }
  };

  var nextSlide = function () {
    if (count < items-3) {
      slideList.style.left = "-" + count * sliderWidth + "px";
      count++;
    }
    else if (count = items-3) {
      slideList.style.left = "0px";
      count = 1;
    }
  };

  document.querySelector(".sliderNext").addEventListener("click", function () {
    nextSlide();
  });

  document.querySelector(".sliderPrev").addEventListener("click", function () {
    prevSlide();
  });

};
galleryResponsive();





